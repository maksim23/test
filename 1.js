class TransformToTree {
    constructor() {
        this._idx = 0;
        this._map = [];
        this._result = [];
    }
    
    _createMap(data) {
        data.forEach((el, idx) => {
            this._map[el.id] = idx;
            el.children = [];
        })
        return true;
    }

    _createTree(data) {
        data.forEach(el => {
            if (el.parent !== 0) {
                data[this._map[el.parent]].children.push(el);
            } else {
                this._result.push(el);
            }
        })
    }

    _print() {
        console.dir(this._result, {depth: 10});
    }



    run(data) {
        this._createMap(data);
        this._createTree(data);
        this._print();
    }
}

let test = [
    {id: 1, parent: 0},
    {id: 2, parent: 4},
    {id: 3, parent: 1},
    {id: 4, parent: 1}
]

let t = new TransformToTree();
t.run(test)