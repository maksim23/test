class AuthService {
    async sygnUp(email, password) {
        // создаем хэш пароля
        // создаем пользователя, в поле пароль пишем хэшированные данные
    }

    async generateToken(user) {
        // создаем data с полями юзера(не скретными типо id, name, email)
        // создаем JWT token
    }

    async login(email, password) {
        // ищем юзера
        // если найден, сравниваем хэш пароля пришедшего и хэш пароля в базе
    }
}

/*
    клиенту нужен будет токен JWT
    нужен middleware
*/

const isAuth = (req) => {
    if (req,headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }
}

module.exports = jwt({
    secret: 'secret key',
    userProperty: 'user token',
    getToken: isAuth
})


// второй middleware для добавление инфы по юзеру

const attachUser = (req, res, next) => {
    try {
        const decodeUser = req.token.data;
        const user = await UserModel.findOne({
            _id: decodeUser._id
        })

        if (!user) {
            res.status(401).end();
        }
        req.currentUser = user;
        return next();
    } catch(e) {
        return res.json(e).status(500);
    }
}

// можно еще добавить проверку роли, допустим админ или нет

const checkRole = (role) => {
    return (req, res, next) => {
        if(req.currentUser.role !== role) {
            return res.status(401).end();
          } else {
            return next();
          }
    }
}

/*

            Поиск юзера

            Юзер существует?  ->  нет -> выход 

                да

            Хэшировать пароль

                пароль совпал?  -> нет -> выход
            
                совпал

            Сгенерировать JWT
*/

