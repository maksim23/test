module.exports = (app) => {
    app.post('/login', async (req, res) => {
        const email = req.body.user.email
        const password = req.body.user.password;

        try {
            const authService = new AuthService()
            const { user, token } = await authService.login(email, password);
            return res.status(200).json({user, token}).end()
        } catch(e) {
            return res.json(e).status(500).end();
        }
    })

    app.post('/greeting', isAuth, attachUser, async (req, res) => {
        // с middleware auth проверяет существует пользователь c таким токеном или нет
        // attachUser возьмет и добавит его в req
    })
}