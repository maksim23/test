class Controller {
    constructor(ctx) {}
}

class Ctrl extends Controller {
    constructor(ctx) {
        super(ctx);
    }

    async run() {
        // здесь можно доставать данные чанками, а не целиком
        // реализация была на собеседовании
        const orders = await Order.Model.find().select('id user_id');
        const users = [];
        let count = 0;

        // здесь необходимо подготовить данные по заказам таким образом, 
        // чтобы можно было сделать find один раз
        // User.Model.find( { $or: [ {id: 1}, {id: 2}, {id: 3} ] } )
        // на выходе мы вернем массив юзеров и всё будет здорово
        // let orderIds = orders.map(order => ({id: order.user_id}));
        for (let order of orders) {
            const user = await User.Model.findOne({id: order.user_id});
            if (user)
                users.push(user)
        }

        // тут не особо круто что мы обновляем наши сущности по отдельности
        // к сожалению не могу найти нужный метод в монгусе, но если переводить на язык sql, то
        // сделать апдейт по сущностям, установив им problem = true, где problem = false и где user.id = 1 or user.id = 2 и т.д
        // монгус должен возвращать количество записей по которым прошел апдейт

        // > db.count.update({x : 1}, {$inc : {x : 1}}, false, true)
        // > db.runCommand({getLastError : 1})
        // {
        // "err" : null,
        // "updatedExisting" : true,
        // "n" : 5,
        // "ok" : true
        // }
        for (let user of users) {
            if (user.problem === false)
                count++;
            user.problem = true;
            await user.save();
        }

        return { count };

    }
}

